<?php
/**
 * Template part for displaying posts in the frontpage
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Influencer_Codewing
 */

?>

<div class="news-block" itemscope itemtype="http://schema.org/Article">
    <?php if ( has_post_thumbnail() ) { ?>
        <figure class="news-block-img">
            <?php the_post_thumbnail( 'influencer_codewing_frontpage_blog' ); ?>
        </figure>
    <?php } ?>
    <div class="news-content-wrap">
        <h3 class="news-block-title" itemprop="headline">
            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
        </h3>
        <div class="entry-meta">
            <span class="byline" itemprop="author">
                <?php esc_html_e( 'by', 'influencer-codewing' ); ?> <a itemprop="name" class="author vcard" href="<?php the_permalink(); ?>"><?php the_author(); ?></a>
            </span>
            <span class="comments"><?php echo absint(get_comments_number() ); esc_html_e( ' Comments', 'influencer-codewing' ); ?></span>
        </div>
        <div class="news-block-desc" itemprop="text">
           <?php the_excerpt(); ?> 
        </div>
        <a href="<?php the_permalink(); ?>" class="readmore" itemprop="mainEntityOfPage"><?php esc_html_e( 'Continue Reading', 'influencer-codewing' ); ?> <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/readmore-arrow.png" alt="<?php _e( 'readmore arrow', 'influencer-codewing' ); ?>"></a>
    </div>
</div>