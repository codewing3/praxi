<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Influencer_Codewing
 */

get_header();
$image = get_theme_mod( 'influencer_codewing_search_header_image' );
?>
<div class="page-header" style="background: url(<?php echo esc_url( $image ); ?>); ">
    <h1 class="page-title">
        <?php
        /* translators: %s: search query. */
        printf( esc_html__( 'Search Results for: %s', 'influencer-codewing' ), '<span>' . get_search_query() . '</span>' );
        ?>
    </h1>
</div><!-- .page-header -->
<div class="cm-wrapper">
    <div id="primary" class="content-area">	
        <main id="main" class="site-main">
            <?php if ( have_posts() ) : ?>
                <?php
                /* Start the Loop */
                while ( have_posts() ) :
                    the_post();
                    /**
                     * Run the loop for the search to output the results.
                     * If you want to overload this in a child theme then include a file
                     * called content-search.php and that will be used instead.
                     */
                    get_template_part( 'template-parts/content', get_post_type() );
                endwhile;
                the_posts_navigation();
            else :
                get_template_part( 'template-parts/content', 'none' );
            endif;
            ?>
        </main><!-- #main -->
    </div>
    <?php get_sidebar(); ?>       
</div>
<?php
get_footer();
