
jQuery(document).ready(function($){

	var tickerHeight = $('.site-header .header-ticker').height();
	$('body').css('margin-top', tickerHeight);

	$('.header-ticker span.ticker-close').click(function(){
		$('.ticker-title-wrap').slideToggle();
		$('body').toggleClass('ticker-off');
		if($('body').hasClass('ticker-off')){
			$('body.ticker-off').css('margin-top', 0);
		}else {
			$('body').css('margin-top', tickerHeight);
		}
	});

	$('.scroll-to-top span').click(function(){
		$('html, body').animate({
			scrollTop: 0
		}, 600);
	});

	$(document).on('click', 'a.scroll-down', function(event){
 		//console.log($.attr(this, 'href').indexOf('#'));
 		if($.attr(this, 'href').indexOf('#')==0){
 			event.preventDefault();

 			$('html, body').animate({
 				scrollTop: $( $.attr(this, 'href') ).offset().top
 			}, 600);
 		}
 	});

 	 // Stat Counter
 	 $('.counter').counterUp({
 	 	delay: 20,
 	 	time: 1000
 	 });

 	 var winWidth = $(window).width();
 	 var wrapWidth = $('.cm-wrapper').width();
 	 var subTotal = parseInt(winWidth) - parseInt(wrapWidth);
 	 var halfVal = parseInt(subTotal) / 2;
 	 if(winWidth >=641) {
 	 	$('.story-feat-wrap .widget-wrap').css('padding-right', halfVal);
 	 }

 	 //stiky social share

 	 if(winWidth> 980) {
 	 	var pageHeaderHeight = $('.single .page-header').innerHeight();
 	 	var totalPHeight = parseInt(pageHeaderHeight) + 60;
 	 	$('.side-social-share').css('top', totalPHeight);

 	 	$(window).scroll(function(){
 	 		if($(this).scrollTop() > totalPHeight){
 	 			$('.side-social-share').css('position', 'fixed');
 	 			$('.side-social-share').css('top', 60);
 	 		}else{
 	 			$('.side-social-share').css('position', 'absolute');
 	 			$('.side-social-share').css('top', totalPHeight);
 	 		}
 	 	});
 	 }

 	 //widget team js
 	 $('.rtc-team-holder').click(function(){
 	 	$(this).siblings('.rtc-team-holder-model').addClass('show');
 	 	$(this).siblings('.rtc-team-holder-model').css('display', 'block');
 	 });

 	 $('.close_popup').click(function(e){
 	 	e.preventDefault();
 	 	$(this).parent('.rtc-team-holder-model').removeClass('show');
 	 	$(this).parent().css('display', 'none');
 	 });

 	 //responsive menu

 	 if(winWidth <= 980){
 	 	$('.toggle-button').click(function(){
 	 		$('.main-navigation .nav-menu').slideToggle();
 	 		$('.main-navigation').toggleClass('menu-active');
 	 	});

 	 	$('.main-navigation ul li.menu-item-has-children').append('<span class="fa fa-chevron-down"></span>');

 	 	$('.menu-item-has-children > span').on('click', function(){
 	 		$(this).siblings('.menu-item-has-children ul').stop(true, false, true).slideToggle();
 	 		$(this).toggleClass('submenu-toggle');
 	 	});
 	 }

 	});