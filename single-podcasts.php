<?php
get_header();
?>

<div id="content" class="site-content">
<?php 
    /**
     * Displaying single page header with image
     */
     influencer_codewing_single_page_header(); 
     ?>
    <div class="cm-wrapper">
        <div id="primary" class="content-area">
            <main id="main" class="site-main">
                <div class="article-group">
                    <?php if ( have_posts() ){ 
                        while ( have_posts () ){
                            the_post(); 
                            get_template_part( 'template-parts/content', 'single' ); 
                        } 
                    } ?>
                </div>
                <?php 
                /**
                 * Displaying author box and newsletter
                 */
                influencer_codewing_author_newsletter_box(); 
                
                the_post_navigation( [
                    'prev_text'          => sprintf( esc_html__( '%1s &#129120; Previous Posts %2s', 'influencer-codewing'), '<span class="pagination-txt">', '</span><h4>%title</h4>'),
                    'next_text'          => sprintf( esc_html__( '%1s Next Posts &#129122; %2s', 'influencer-codewing'), '<span class="pagination-txt">', '</span><h4>%title</h4>'),
                ]);           
                
                if( comments_open() || get_comments_number() ){ 
                    comments_template();
                }                            
                ?>
            </main>
        </div>
    </div>
</div>
<?php get_footer(); ?>