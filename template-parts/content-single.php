<?php
/**
 * Template part for displaying single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Influencer_Codewing
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="side-social-share">
        <img src="<?php echo esc_url( get_template_directory_uri() . '/images/side-social-share.jpg' ); ?>" alt="<?php esc_html_e( 'side social', 'influencer-codewing' ); ?>">
    </div>
    <?php the_content(); ?>
    <div class="tag-share-wrap">
        <div class="tag-block">
            <?php the_tags( '<span>', '  &#183;  ', '</span>' ); ?>
        </div>
        <div class="bottom-share-block">
            <img src="<?php echo esc_url( get_template_directory_uri() . "/images/bottom-share.jpg" ); ?>" alt="<?php esc_html_e( 'bottom share', 'influencer-codewing' ); ?>">
        </div>
    </div>
</article>
