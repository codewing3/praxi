<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Influencer_Codewing
 */

?>
<div class="article-group">
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> >
        <div class="entry-content" itemprop="text">
            <?php
            the_content();

            wp_link_pages( array(
                'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'influencer-codewing' ),
                'after'  => '</div>',
            ) );
            ?>
        </div><!-- .entry-content -->
        <?php if ( get_edit_post_link() ) { ?>
            <footer class="entry-footer">
                <?php
                edit_post_link(
                    sprintf(
                        wp_kses(
                            /* translators: %s: Name of current post. Only visible to screen readers */
                            __( 'Edit <span class="screen-reader-text">%s</span>', 'influencer-codewing' ),
                            array(
                                'span' => array(
                                    'class' => array(),
                                ),
                            )
                        ),
                        get_the_title()
                    ),
                    '<span class="edit-link">',
                    '</span>'
                );
                ?>
            </footer><!-- .entry-footer -->
        <?php } ?>
	</article><!-- #post-<?php the_ID(); ?> -->
</div>
