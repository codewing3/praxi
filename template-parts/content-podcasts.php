<?php global $post;
$author_ID = $post->post_author;
?>
<article>
    <figure><a href="<?php echo esc_url( get_author_posts_url( $author_ID ) ); ?>" class="entry-image"><?php echo get_avatar( $author_ID, 338, '', 'avatar', []); ?></a></figure>
    <div class="entry-content">
        <header class="entry-header">
            <div class="entry-meta">
                <div class="tag-group">
                    <span><?php echo esc_html( get_the_date( 'F j, Y') ); ?></span>
                </div>
                <div class="article-author">
                    <a href="#" class="url"><?php the_author(); ?></a>
                </div>
                <span class="article-author-pos"><?php the_author_meta( 'description' ); ?></span>
            </div>
            <a href="<?php the_permalink(); ?>"><h2 class="entry-title"><?php the_title(); ?></h2></a>
        </header>
        <p><?php the_excerpt(); ?></p>
        <img src="<?php echo esc_url( get_template_directory_uri() . '/images/audio-player.jpg' ); ?>" alt="<?php esc_html_e( 'audio player', 'influencer-codewing' ); ?>">
    </div>
</article>