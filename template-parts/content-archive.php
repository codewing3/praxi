<?php
/**
 * Template part for displaying archive posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Influencer_Codewing
 */
global $post;
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> itemscope itemtype="http://schema.org/Article">
    <header class="entry-header">
        <h2 class="entry-title" itemprop="headline"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
        <div class="entry-meta">
                <span class="posted-on" itemprop="datePublished dateModified">
                    <span><?php esc_html_e( 'Last Updated:', 'influencer-codewing' ); ?></span> 
                        <a href="<?php echo esc_url( get_day_link( get_post_time( 'Y' ), get_post_time( 'm' ), get_post_time( 'j' ) ) ); ?>">
                        <?php esc_html_e( 'on ', 'influencer-codewing' ); ?><time class="updated published"><?php echo esc_html( get_the_date( 'F j, Y') ); ?></time>
                        </a>
                </span>
            <span class="byline" itemprop="author">
                <?php esc_html_e( 'by', 'influencer-codewing' ); ?>
                <span class="author vcard">
                    <a href="<?php echo esc_url( get_author_posts_url( $post->post_author ) ); ?>" class="url" itemprop="name"><?php the_author(); ?></a>
                </span>
            </span>
            <span class="comment-box">
                <span class="comment-count"><?php echo absint(get_comments_number() ); ?></span><?php esc_html_e( ' Comments', 'influencer-codewing' ); ?>
            </span>
        </div>
    </header>
    <div class="entry-content">
        <?php if ( has_post_thumbnail() ) { ?>
            <a href="<?php the_permalink(); ?>" class="entry-image" itemprop="thumbnailUrl"><?php the_post_thumbnail( 'influencer_codewing_blog_grid_third_layout' ); ?></a>
        <?php } ?>
        <?php the_excerpt(); ?>
        <a href="<?php the_permalink(); ?>" class="readmore" itemprop="MainEntityOfPage"><?php esc_html_e( 'Continue Reading', 'influencer-codewing' ); ?></a>
    </div>
</article>