<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Influencer_Codewing
 */

?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> itemscope itemtype="http://schema.org/Article">
    <?php 
    global $post;
    $author_ID  =   $post->post_author;
    $layout = get_theme_mod( 'influencer_codewing_index_layout_handle' );
    if ( $layout === 'one' ) { 
    ?>
        <?php if ( has_post_thumbnail() ) { ?>
            <a href="<?php the_permalink(); ?>" class="entry-image" itemprop="thumbnailUrl"><?php the_post_thumbnail( 'influencer_codewing_blog_grid_layout' ); ?></a>
        <?php } ?>
        <div class="entry-content">
    <?php } 
    if ( $layout ) { ?>
        <header class="entry-header">
            <h2 class="entry-title" itemprop="headline"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
            <div class="entry-meta">
                <?php if ( $layout !== 'one' ) { ?> 
                    <span class="posted-on" itemprop="datePublished dateModified">
                        <span><?php esc_html_e( 'Last Updated:', 'influencer-codewing' ); ?></span> 
                            <a href="<?php echo esc_url( get_day_link( get_post_time( 'Y' ), get_post_time( 'm' ), get_post_time( 'j' ) ) ); ?>">
                            <?php esc_html_e( 'on ', 'influencer-codewing' ); ?><time class="updated published"><?php echo esc_html( get_the_date( 'F j, Y') ); ?></time>
                            </a>
                    </span>
                <?php } ?>
                <span class="byline" itemprop="author">
                    <?php esc_html_e( 'by', 'influencer-codewing' ); ?>
                    <span class="author vcard">
                        <a href="<?php echo esc_url( get_author_posts_url( $author_ID ) ); ?>" class="url" itemprop="name"><?php the_author(); ?></a>
                    </span>
                </span>
                <span class="comment-box">
                    <span class="comment-count"><?php echo absint(get_comments_number() ); ?></span><?php esc_html_e( ' Comments', 'influencer-codewing' ); ?>
                </span>
            </div>
        </header>
    <?php }
    if ( $layout !== 'one' ) { ?>
        <div class="entry-content">
        <?php if ( has_post_thumbnail() ) { ?>
            <?php $thumbnail = ( $layout === 'two' ) ? the_post_thumbnail( 'influencer_codewing_blog_grid_second_layout' ) : the_post_thumbnail( 'influencer_codewing_blog_grid_third_layout' ); ?>
            <a href="<?php the_permalink(); ?>" class="entry-image" itemprop="thumbnailUrl"><?php echo $thumbnail; ?></a>
        <?php } ?>
    <?php }
    if ( $layout ) { ?>
            <?php the_excerpt(); ?>
            <a href="<?php the_permalink(); ?>" class="readmore" itemprop="MainEntityOfPage"><?php esc_html_e( 'Continue Reading', 'influencer-codewing' ); ?></a>
        </div>
    <?php } ?>
</article>
