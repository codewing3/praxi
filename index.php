<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Influencer_Codewing
 */

get_header();
?>

<div id="content" class="site-content">
	<?php 
	/**
	 * Displaying header for the blog page
	*/
	influencer_codewing_blog_page_header(); 
	
 ?>
		<div class="cm-wrapper">
			<div id="primary" class="content-area">	
				<main id="main" class="site-main">
					<?php if ( have_posts() ) {				 
						$layout = get_theme_mod( 'influencer_codewing_index_layout_handle' ); 
						if ( $layout === 'one' || $layout === 'two' ) {
							$add_class = 'grid-layout';
						} else {
							$add_class = '';
						}
						?>						
						<div class="article-group <?php echo esc_attr( $add_class ); ?>">
							<?php					
							/* Start the Loop */
							while ( have_posts() ) {
								the_post();
								/*
								* Include the Post-Type-specific template for the content.
								* If you want to override this in a child theme, then include a file
								* called content-___.php (where ___ is the Post Type name) and that will be used instead.
								*/
								get_template_part( 'template-parts/content', get_post_type() );						
							} ?>
						</div>				
						<?php the_posts_pagination( array(
							'prev_text'     		=>  __( '&#129120; Previous', 'influencer-codewing' ),
							'next_text'				=>	__( 'Next &#129122;', 'influencer-codewing'),
							'before_page_number'    =>  '<span class="meta-nav screen-reader-text">' . __( 'Page', 'influencer-codewing' ) . '</span>'
						)); 	
					} else { 
						get_template_part( 'template-parts/content', 'none' );
					} ?>		
				</main><!-- #main -->
			</div>
			<?php ( $layout === 'three' ) ? get_sidebar() : ''; ?>
		</div>
</div><!-- .site-content -->
<?php

get_footer();