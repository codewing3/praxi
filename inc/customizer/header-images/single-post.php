<?php

function influencer_codewing_single_header_image($wp_customize)
{

    $wp_customize->add_section( 'influencer_codewing_single_header_images', [
        'panel' => 'influencer_codewing_header_settings',
        'title' => esc_html__('Header Image for Single Posts', 'influencer-codewing')
    ]);

    $wp_customize->add_setting( 'influencer_codewing_single_header_handle', [
        'default'           =>  'yes',
        'sanitize_callback' =>  'influencer_codewing_sanitize_checkbox'
    ]);

    $wp_customize->add_control( new WP_Customize_Control(
        $wp_customize,
        'influencer_codewing_single_header_handle',
        array(
            'priority'  =>  1,
            'section'   =>  'influencer_codewing_single_header_images',
            'label'     =>  esc_html__('Display a new header image which overrides featured image', 'influencer-codewing'),
            'type'      =>  'checkbox',
            'choices'   =>  [
                'yes'   =>  'Yes'
            ]
        )
    ));

    $wp_customize->add_setting( 'influencer_codewing_single_header_img', [
        'default'           =>          get_theme_file_uri('images/single-header-bg.jpg'),
        'sanitize_callback' =>          'esc_url_raw'
    ]);

    $wp_customize->add_control( new WP_Customize_Image_Control(
        $wp_customize,
        'influencer_codewing_single_header_img',
        array(
            'label'   => esc_html__( 'Upload image for single post', 'influencer-codewing' ),
            'section' => 'influencer_codewing_single_header_images'
        )
    ));
}
