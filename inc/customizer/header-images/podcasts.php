<?php

function influencer_codewing_podcast_header_image( $wp_customize ) {
    
    $wp_customize->add_section( 'influencer_codewing_podcast_header', [
        'panel' => 'influencer_codewing_header_settings',
        'title' => esc_html__( 'Header Image for Podcast' )
    ]);

    $wp_customize->add_setting( 'influencer_codewing_podcast_header_title', [
        'default'           => esc_html__( 'Listen to our awesome Podcasts', 'influencer-codewing' ),
        'sanitize_callback' => 'sanitize_text_field'
    ]);

    $wp_customize->add_control( new WP_Customize_Control(
        $wp_customize,
        'influencer_codewing_podcast_header_title',
        array(
            'section' => 'influencer_codewing_podcast_header',
            'label'   => esc_html__( 'Enter title here', 'influencer-codewing' ),
            'type'    => 'text'
        )
    ));

    $wp_customize->add_setting( 'influencer_codewing_podcast_header_image', [
        'default'           => get_theme_file_uri( 'images/blog-grid-img4.jpg' ),
        'sanitize_callback' => 'esc_url_raw'
    ]);

    $wp_customize->add_control( new WP_Customize_Image_Control(
        $wp_customize,
        'influencer_codewing_podcast_header_image',
        array(
            'section' => 'influencer_codewing_podcast_header',
            'label'   => esc_html__( 'Upload header image for the Podcast', 'influencer-codewing' ),
        )
    ));

}