<?php 

function influencer_codewing_blog_header_image( $wp_customize ) {

    $wp_customize->add_section( 'influencer_codewing_blog_header_images', [
        'panel'         =>         'influencer_codewing_header_settings',
        'title'         =>          esc_html__( 'Header Image for Blog Page', 'influencer-codewing' )
    ]);

    $wp_customize->add_setting( 'influencer_codewing_blog_header_handle', [
        'default'           =>  'yes',
        'sanitize_callback' =>  'influencer_codewing_sanitize_checkbox'
    ]);

    $wp_customize->add_control( new WP_Customize_Control(
        $wp_customize,
        'influencer_codewing_blog_header_handle',
        array(
            'priority'  =>  1,
            'section'   =>  'influencer_codewing_blog_header_images',
            'label'     =>  esc_html__( 'Display Header Image', 'influencer-codewing' ),
            'type'      =>  'checkbox',
            'choices'   =>  [
                'yes'   =>  'Yes'
            ]
        )
    ));

    $wp_customize->add_setting( 'influencer_codewing_index_header_image', [
        'default'           =>       get_theme_file_uri( 'images/blog-grid-img4.jpg' ),
        'sanitize_callback' =>      'esc_url_raw'
    ]);

    $wp_customize->add_control( new WP_Customize_Image_Control(
        $wp_customize,
        'influencer_codewing_index_header_image',
        array(
            'section'       =>  'influencer_codewing_blog_header_images',
            'label'         =>  esc_html__( 'Upload header image for the blog page', 'influencer-codewing' ),
        )
    ));

    $wp_customize->add_setting( 'influencer_codewing_index_header_image_title', [
        'default'           =>       esc_html__('Content Marketing Article', 'influencer-codewing' ),
        'sanitize_callback' =>      'sanitize_text_field'
    ]);

    $wp_customize->add_control( new WP_Customize_Control(
        $wp_customize,
        'influencer_codewing_index_header_image_title',
        array(
            'section'       =>  'influencer_codewing_blog_header_images',
            'label'         =>  esc_html__( 'Write title to be displayed in the image', 'influencer-codewing' ),
            'type'          =>  'text'
        )
    ));
}