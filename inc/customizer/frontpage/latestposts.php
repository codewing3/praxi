<?php

function influencer_codewing_frontpage_latest_posts( $wp_customize ) {

    $wp_customize->add_section( 'influencer_codewing_latest_posts', [
        'panel'     =>      'influencer_codewing_frontpage_settings',
        'title'     =>      esc_html__( 'Blog Settings', 'influencer-codewing' )
    ]);

    $wp_customize->add_setting( 'influencer_codewing_latest_posts_title', [
        'default'           =>   esc_html__( 'Latest Posts', 'influencer-codewing' ),
        'sanitize_callback' =>  'sanitize_text_field',
        'transport'         =>  'postMessage'
    ]);

    $wp_customize->add_control( new WP_Customize_Control(
        $wp_customize,
        'influencer_codewing_latest_posts_title',
        array(
            'label'         =>  esc_html__( 'Enter Title here', 'influencer-codewing'),
            'section'       =>  'influencer_codewing_latest_posts',
            'type'          =>  'text'
        )
    ));

    $wp_customize->add_setting( 'influencer_codewing_latest_posts_subtitle', [
        'default'           =>  esc_html__( 'TIPS FOR GETTING THINGS DONE', 'influencer-codewing' ),
        'sanitize_callback' =>  'sanitize_text_field',
        'transport'         =>  'postMessage'    
    ]);

    $wp_customize->add_control( new WP_Customize_Control(
        $wp_customize,
        'influencer_codewing_latest_posts_subtitle',
        array(
            'label'         =>  esc_html__( 'Enter Subtitle here', 'influencer-codewing'),
            'section'       =>  'influencer_codewing_latest_posts',
            'type'          =>  'text'
        )
    ));

    $wp_customize->add_setting( 'influencer_codewing_latest_posts_button', [
        'default'           =>      esc_html__( 'More From The Blog', 'influencer-codewing' ),
        'sanitize_callback' =>  'sanitize_text_field',
        'transport' => 'postMessage'
    ]);

    $wp_customize->add_control( new WP_Customize_Control(
        $wp_customize,
        'influencer_codewing_latest_posts_button',
        array(
            'label'     => esc_html__( 'Enter text for button here', 'influencer-codewing'),
            'section'   => 'influencer_codewing_latest_posts',
            'type'      => 'text',
        )
    ));
}