<?php

function influencer_codewing_frontpage_banner( $wp_customize ) {

    $wp_customize->add_setting( 'influencer_codewing_frontpage_banner_handle', [
        'default'           =>  'yes',
        'sanitize_callback' =>  'influencer_codewing_sanitize_checkbox'
    ]);

    $wp_customize->add_control( new WP_Customize_Control(
        $wp_customize,
        'influencer_codewing_frontpage_banner_handle',
        array(
            'priority'  =>  1,
            'section'   =>  'header_image',
            'label'     =>  esc_html__( 'Display Banner', 'influencer-codewing' ),
            'type'      =>  'checkbox',
            'choices'   =>  [
                'yes'   =>  esc_html__( 'Yes', 'influencer-codewing' )
            ]
        )
    ));
}