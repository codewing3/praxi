<?php

function influencer_codewing_frontpage_widgets( $wp_customize ){
    $logo_wid = (object) $wp_customize->get_section( 'sidebar-widgets-logo-section' );
	$logo_wid->panel = 'influencer_codewing_frontpage_settings';
    $logo_wid->priority = 15;

    $feature_wid = (object) $wp_customize->get_section( 'sidebar-widgets-featured-page' );
	$feature_wid->panel = 'influencer_codewing_frontpage_settings';
    $feature_wid->priority = 20;

    $icon_wid = (object) $wp_customize->get_section( 'sidebar-widgets-front-page-icon-text' );
	$icon_wid->panel = 'influencer_codewing_frontpage_settings';
    $icon_wid->priority = 25;

    $testimonial_wid = (object) $wp_customize->get_section( 'sidebar-widgets-front-page-testimonial' );
	$testimonial_wid->panel = 'influencer_codewing_frontpage_settings';
    $testimonial_wid->priority = 30;

    $cta_wid = (object) $wp_customize->get_section( 'sidebar-widgets-front-page-cta' );
	$cta_wid->panel = 'influencer_codewing_frontpage_settings';
    $cta_wid->priority = 35;
}