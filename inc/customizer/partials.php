<?php 
/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function influencer_codewing_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function influencer_codewing_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

function influencer_codewing_customize_partial_latest_posts_title() {
	echo esc_html( get_theme_mod( 'influencer_codewing_latest_posts_title' ) );
}

function influencer_codewing_customize_partial_latest_posts_subtitle() {
	echo esc_html( get_theme_mod( 'influencer_codewing_latest_posts_subtitle' ) );
}

function influencer_codewing_customize_partial_latest_posts_button() {
    echo esc_html( get_theme_mod( 'influencer_codewing_latest_posts_button' ) );
}