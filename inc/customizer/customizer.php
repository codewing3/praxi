<?php
/**
 * Influencer Codewing Theme Customizer
 *
 * @package Influencer_Codewing
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function influencer_codewing_customize_register( $wp_customize ) {

	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';


	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial(
			'blogname',
			array(
				'selector'        => '.site-title a',
				'render_callback' => 'influencer_codewing_customize_partial_blogname',
			)
		);
		$wp_customize->selective_refresh->add_partial(
			'blogdescription',
			array(
				'selector'        => '.site-description',
				'render_callback' => 'influencer_codewing_customize_partial_blogdescription',
			)
		);
		$wp_customize->selective_refresh->add_partial(
			'influencer_codewing_latest_posts_title',
			array(
				'selector'         => '.section-title',
				'render_callback'  => 'influencer_codewing_customize_partial_latest_posts_title',
				'fallback_refresh' => false,
			)
		);
		$wp_customize->selective_refresh->add_partial(
			'influencer_codewing_latest_posts_subtitle',
			array(
				'selector'         => '.section-subtitle',
				'render_callback'  => 'influencer_codewing_customize_partial_latest_posts_subtitle',
				'fallback_refresh' => false,
			)
		);
		$wp_customize->selective_refresh->add_partial(
			'influencer_codewing_latest_posts_button',
			array(
				'selector'         => '.bttn',
				'render_callback'  => 'influencer_codewing_customize_partial_latest_posts_button',
				'fallback_refresh' => false,
			)
		);
	}

	/**
	 * Adding panels
	 */
	$wp_customize->add_panel( 'influencer_codewing_frontpage_settings', [
		'capability'		=>	'edit_theme_options',
		'title'				=>	esc_html__( 'Front Page Settings', 'influencer-codewing' ),
		'description'		=>	esc_html__( 'Theme Settings', 'influencer-codewing' ),
		'priority'			=> 125,
	]);

	$wp_customize->add_panel( 'influencer_codewing_about_us_page_settings', [
		'capability'		=>	'edit_theme_options',
		'title'				=>	esc_html__( 'About-us Page Settings', 'influencer-codewing' ),
		'description'		=>	esc_html__( 'Setting for about us page template', 'influencer-codewing' ),
		'priority'			=>	125,
	]);

	$wp_customize->add_panel( 'influencer_codewing_header_settings', [
		'capability'		=>	'edit_theme_options',
		'title'				=>	esc_html__( 'Header Images Settings', 'influencer-codewing' ),
		'description'		=>	esc_html__( 'Choose header image for different blog pages/posts', 'influencer-codewing' ),
		'priority'			=>	130,
	]);

	$wp_customize->add_panel( 'influencer_codewing_layout_settings', [
		'capability'		=>	'edit_theme_options',
		'title'				=>	esc_html__( 'Layout Settings', 'influencer-codewing' ),
		'description'		=>	esc_html__( 'Choose layout for blog list and single post/pages', 'influencer-codewing' ),
		'priority'			=>	135,
	]);

	$wp_customize->get_section( 'header_image' )->panel 	=	'influencer_codewing_frontpage_settings';
	$wp_customize->get_section( 'header_image' )->priority 	=	10;
	/**
	 * Including customizer files
	 */
	influencer_codewing_footer_customizer( $wp_customize );
	influencer_codewing_frontpage_banner( $wp_customize );
	influencer_codewing_frontpage_latest_posts( $wp_customize );
	influencer_codewing_blog_header_image( $wp_customize );
	influencer_codewing_single_header_image( $wp_customize );
	influencer_codewing_index_layout( $wp_customize );
	influencer_codewing_single_layout( $wp_customize );
	influencer_codewing_search_header_image( $wp_customize );
	influencer_codewing_404_header_image( $wp_customize );
	influencer_codewing_archive_header_image( $wp_customize );
	influencer_codewing_page_header_image( $wp_customize );
	influencer_codewing_Podcast_header_image( $wp_customize );
	influencer_codewing_about_us_page( $wp_customize );
	influencer_codewing_about_us_widgets( $wp_customize );
	influencer_codewing_frontpage_widgets( $wp_customize );
}
add_action( 'customize_register', 'influencer_codewing_customize_register' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function influencer_codewing_customize_preview_js() {
	wp_enqueue_script( 'influencer-codewing-customizer', get_template_directory_uri() . 'inc/js/customizer.js', array( 'customize-preview' ), _INFLUENCER_CODEWING, true );
}
add_action( 'customize_preview_init', 'influencer_codewing_customize_preview_js' );