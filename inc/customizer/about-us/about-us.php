<?php 

function influencer_codewing_about_us_page( $wp_customize ) {

    $wp_customize->add_section( 'influencer_codewing_about_us_page', [
        'panel'    => 'influencer_codewing_about_us_page_settings',
        'title'    => esc_html__( 'Header Image and About us content and image settings', 'influencer-codewing' ),
        'priority' => 10,
    ]);

    $wp_customize->add_setting( 'influencer_codewing_about_us_header_image', [
        'default'           =>       get_theme_file_uri( 'images/blog-grid-img4.jpg' ),
        'sanitize_callback' =>      'esc_url_raw'
    ]);

    $wp_customize->add_control( new WP_Customize_Image_Control(
        $wp_customize,
        'influencer_codewing_about_us_header_image',
        array(
            'section'       =>  'influencer_codewing_about_us_page',
            'label'         =>  esc_html__( 'Upload header image here', 'influencer-codewing' ),
        )
    ));

    $wp_customize->add_setting( 'influencer_codewing_about_us_page_handle', [
        'default'           =>  'yes',
        'sanitize_callback' =>  'influencer_codewing_sanitize_checkbox'
    ]);

    $wp_customize->add_control( new WP_Customize_Control(
        $wp_customize,
        'influencer_codewing_about_us_page_handle',
        array(
            'section'   =>  'influencer_codewing_about_us_page',
            'label'     =>  esc_html__( 'Display Content', 'influencer-codewing' ),
            'type'      =>  'checkbox',
            'choices'   =>  [
                'yes'   =>  esc_html__( 'Yes', 'influencer-codewing' ),
                'no'    =>  esc_html__( 'No', 'influencer-codewing' )
            ]
        )
    ));

    $wp_customize->add_setting( 'influencer_codewing_about_us_page_image_handle', [
        'default'           =>  'yes',
        'sanitize_callback' =>  'influencer_codewing_sanitize_checkbox'
    ]);

    $wp_customize->add_control( new WP_Customize_Control(
        $wp_customize,
        'influencer_codewing_about_us_page_image_handle',
        array(
            'section'   =>  'influencer_codewing_about_us_page',
            'label'     =>  esc_html__( 'Display image beside content', 'influencer-codewing' ),
            'type'      =>  'checkbox',
            'choices'   =>  [
                'yes'   =>  esc_html__( 'Yes', 'influencer-codewing' ),
                'no'    =>  esc_html__( 'No', 'influencer-codewing' )
            ]
        )
    ));

    $wp_customize->add_setting( 'influencer_codewing_about_us_content_image', [
        'default'           =>       get_theme_file_uri( 'images/story-img.jpg' ),
        'sanitize_callback' =>      'esc_url_raw'
    ]);

    $wp_customize->add_control( new WP_Customize_Image_Control(
        $wp_customize,
        'influencer_codewing_about_us_content_image',
        array(
            'section'       =>  'influencer_codewing_about_us_page',
            'label'         =>  esc_html__( 'Upload content image here', 'influencer-codewing' ),
        )
    ));
    $wp_customize->add_setting( 'influencer_codewing_about_us_stats_count_bg_image', [
        'default'           =>       get_theme_file_uri( 'images/stat-counter-bg.jpg' ),
        'sanitize_callback' =>      'esc_url_raw'
    ]);

    $wp_customize->add_control( new WP_Customize_Image_Control(
        $wp_customize,
        'influencer_codewing_about_us_stats_count_bg_image',
        array(
            'section'       =>  'influencer_codewing_about_us_page',
            'label'         =>  esc_html__( 'Upload background image for stats count here', 'influencer-codewing' ),
        )
    ));

}