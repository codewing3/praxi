<?php

function influencer_codewing_about_us_widgets( $wp_customize ){
    
	$media_wid = (object) $wp_customize->get_section( 'sidebar-widgets-media-image' );
	$media_wid->panel = 'influencer_codewing_about_us_page_settings';
    $media_wid->priority = 15;

	$icon_wid = (object) $wp_customize->get_section( 'sidebar-widgets-icon-text' );
	$icon_wid->panel = 'influencer_codewing_about_us_page_settings';
	$icon_wid->priority = 20;

	$stat_wid = (object) $wp_customize->get_section( 'sidebar-widgets-success-stats' );
	$stat_wid->panel = 'influencer_codewing_about_us_page_settings';
	$stat_wid->priority = 25;

	$team_wid = (object) $wp_customize->get_section( 'sidebar-widgets-team-member' );
	$team_wid->panel = 'influencer_codewing_about_us_page_settings';
	$team_wid->priority = 30;
}