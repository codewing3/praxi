<?php 

function influencer_codewing_index_layout( $wp_customize ){

    $wp_customize->add_section( 'influencer_codewing_index_layout_setting', [
        'panel'             =>      'influencer_codewing_layout_settings',
        'title'             =>      esc_html__( 'Blog layout settings', 'influencer-codewing' )
    ]);

    $wp_customize->add_setting( 'influencer_codewing_index_layout_handle', [
        'default'           =>  'one',
        'sanitize_callback' =>  'influencer_codewing_sanitize_radio'
    ]);

    $wp_customize->add_control( new WP_Customize_Control(
        $wp_customize,
        'influencer_codewing_index_layout_handle',
        array(
            'section'   =>  'influencer_codewing_index_layout_setting',
            'label'     =>  esc_html__( 'Choose layout style', 'influencer-codewing' ),
            'type'      =>  'radio',
            'choices'   =>  [
                'one'   =>  esc_html__( 'Grid Layout', 'influencer-codewing' ),
                'two'  => esc_html__( 'Grid Layout 2', 'influencer-codewing' ),
                'three'  => esc_html__( 'No Grid', 'influencer-codewing' )  
            ]
        )
    ));
}