<?php 

function influencer_codewing_footer_customizer( $wp_customize ){
    
    $wp_customize->add_section( 'influencer_codewing_footer_section', [
        'title'             =>  esc_html__( 'Footer Settings', 'influencer-codewing' ),
        'priority'          =>  140
    ]);

    $wp_customize->add_setting( 'influencer_codewing_footer_text', [
        'default'           =>  '',
        'sanitize_callback' =>  'wp_kses_post'
    ]);

    $wp_customize->add_control( new WP_Customize_Control (
        $wp_customize,
        'influencer_codewing_footer_text',
        array(
            'label'         => esc_html__( 'Enter copyright text here', 'influencer-codewing' ),
            'section'       =>  'influencer_codewing_footer_section',
            'type'          => 'textarea'
        )
    ));
}