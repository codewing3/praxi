<?php
/**
 * Content to be displayed in the related posts section
 */
?>
<div class="related-post-block">
    <?php if (has_post_thumbnail()) { ?>
        <?php the_post_thumbnail( 'influencer_codewing_related_posts' ); ?>
    <?php } ?>
    <div class="related-content-wrap">
        <h2 class="related-block-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
        <a href="<?php the_permalink(); ?>" class="readmore"><?php esc_html_e('Continue Reading', 'influencer-codewing'); ?><img src="<?php echo esc_url(get_template_directory_uri() . '/images/readmore-arrow.png'); ?>"></a>
    </div>
    <!--related-content-wrap -->
</div><!--related-article-block -->