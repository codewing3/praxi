<?php
/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function influencer_codewing_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'influencer-codewing' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'influencer-codewing' ),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);

	register_sidebars(
		4,
		array(
			'name'			=> esc_html__( 'Footer %d', 'influencer-codewing' ),
			'id'			=> 'footer',
			'description'   => esc_html__( 'Add widgets here.', 'influencer-codewing' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		)
	);

	$sidebars = [
		'media-image' => array(
			'name'	=>	esc_html__( 'Media Image', 'influencer-codewing' ),
			'id'	=>	'media-image',
			'description'	=>	esc_html__( 'Add "Image" Widget or "Rara:Image Widget" for left content', 'influencer-codewing' )
		),
		'icon-text' => array(
			'name'	=>	esc_html__( 'Icon Text Image', 'influencer-codewing' ),
			'id'	=>	'icon-text',
			'description'	=>	esc_html__( 'Add "Rara:Icon Text" for right content', 'influencer-codewing' )
		),
		'success-stats' => array(
			'name' => esc_html__( 'Widget to display success stats', 'influencer-codewing' ),
			'id'   => 'success-stats',
			'description'	=>	esc_html__( 'Add "Rara: Stats Count Widget"', 'influencer-codewing' )
		),
		'team-member' => array(
			'name'        => esc_html__( 'Widget to display team members', 'influencer-codewing' ),
			'id'          => 'team-member',
			'description' => esc_html__( 'Add "Rara: Team member" Widget here', 'influencer-codewing' )
		),
		'logo-section' => array(
			'name'        => esc_html__( 'Widget to display logo', 'influencer-codewing' ),
			'id'          => 'logo-section',
			'description' => esc_html__( 'Add "Rara: Client Logo" Widget here', 'influencer-codewing' )
		),
		'featured-page' => array(
			'name'        => esc_html__( 'Widget to display featured page', 'influencer-codewing' ),
			'id'          => 'featured-page',
			'description' => esc_html__( 'Add "Rara: A Featured Page Widget" here', 'influencer-codewing' )
		),
		'front-page-icon-text' => array(
			'name'	=>	esc_html__( 'Icon Text Image', 'influencer-codewing' ),
			'id'	=>	'front-page-icon-text',
			'description'	=>	esc_html__( 'Add "Rara:Icon Text" widget', 'influencer-codewing' )
		),
		'front-page-testimonial'=> array(
			'name'        => esc_html__( 'Widget to display testimonial', 'influencer-codewing' ),
			'id'          => 'front-page-testimonial',
			'description' => esc_html__( 'Add "Rara: Testimonial" Widget here', 'influencer-codewing' )
		),
		'front-page-cta'=> array(
			'name'        => esc_html__( 'Widget to display Call to Action', 'influencer-codewing' ),
			'id'          => 'front-page-cta',
			'description' => esc_html__( 'Add "Rara: Call To Action" Widget here', 'influencer-codewing' )
		),
	];

	foreach( $sidebars as $sidebar ) {
		register_sidebar( array(
			'name'	=>	esc_html( $sidebar['name'] ),
			'id'	=>	esc_attr( $sidebar['id'] ),
			'description'	=> esc_html( $sidebar['description'] ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
			)
		);
	}
}
add_action( 'widgets_init', 'influencer_codewing_widgets_init' );