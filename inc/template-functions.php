<?php

/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package Influencer_Codewing
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function influencer_codewing_body_classes( $classes ) {
    $single_layout  =   get_theme_mod( 'influencer_codewing_single_layout_handle' );

    //Add a class of rightsidebar to index layout and archives
    if ( is_search() || is_active_sidebar( 'sidebar-1' ) && is_home()  &&  get_theme_mod( 'influencer_codewing_index_layout_handle' ) === 'three' ) {
        $classes[]  =   'archive rightsidebar';
    }

    //Rightsidebar for single post layout one and pages
    if ( is_archive() || is_page() || ( is_single() && $single_layout === 'one' && is_active_sidebar( 'sidebar-1' ) ) ) {
        $classes[]  =   'rightsidebar';
    }

    if ( is_single() && $single_layout === 'two' ) {
        $classes[]  =   'single-centered-layout full-width';
    }

    if ( is_single() && $single_layout === 'three') {
        $classes[]  =   'single-fullwidth-layout full-width';
    }

    if ( is_page_template( 'about-us.php' ) ) {
        unset( $classes[ array_search( 'rightsidebar', $classes ) ] );
    }

    return $classes;
}
add_filter( 'body_class', 'influencer_codewing_body_classes' );

if ( ! function_exists( 'influencer_codewing_site_branding' ) ) {
    /**
     *Site branding for the header
     */
    function influencer_codewing_site_branding() {
        if (is_front_page()) { ?>
            <h1 class="site-title"><a href="<?php echo esc_url(home_url('/')); ?>" rel="home"><?php bloginfo('name'); ?></a></h1>
        <?php } else { ?>
            <p class="site-title"><a href="<?php echo esc_url(home_url('/')); ?>" rel="home"><?php bloginfo('name'); ?></a></p>
        <?php }
        $influencer_codewing_description = get_bloginfo('description', 'display');
        if ($influencer_codewing_description || is_customize_preview()) { ?>
            <p class="site-description"><?php echo $influencer_codewing_description; ?></p>
        <?php }
    }
}

function influencer_codewing_menu() { ?>
    <div class="nav-wrap">
        <nav id="site-navigation" class="main-navigation">
            <button class="toggle-button" type="button">
                <span class="toggle-bar"></span>
                <span class="toggle-bar"></span>
                <span class="toggle-bar"></span>
            </button>
            <?php
                wp_nav_menu(
                    array(
                        'theme_location' => 'primary',
                        'menu_id'        => 'primary-menu',
                        'menu_class'     => 'nav-menu',
                        'fallback_cb'    => 'influencer_codewing_primary_menu_fallback'
                    )
                );
            ?>
        </nav><!-- #site-navigation -->
    </div>
    <?php
}

if ( ! function_exists( 'influencer_codewing_primary_menu_fallback' ) ) {
    /**
     * Fallback for primary menu
     */
    function influencer_codewing_primary_menu_fallback() {
        if (current_user_can('manage_options')) {
            echo '<ul id="primary-menu" class="nav-menu">';
            echo '<li><a href="' . esc_url(admin_url('nav-menus.php')) . '">' . esc_html__('Click here to add a menu', 'influencer-codewing') . '</a></li>';
            echo '</ul>';
        }
    }
}

if ( ! function_exists( 'influencer_codewing_header' ) ) {
    /**
     * Header for front page
     */
    function influencer_codewing_header() { 
        if ( is_front_page() ) { 
            $class = 'header-1';
        } else {
            $class = '';
        }?>
        <header class="site-header <?php echo $class; ?>" itemscope itemtype="http://schema.org/WPHeader">
            <div class="header-ticker">
                <span class="ticker-close"><i class="fa fa-close"></i></span>
                <div class="ticker-title-wrap">
                    <div class="cm-wrapper">
                        <p class="ticker-title">
                            <?php _e('The ultimate e-mail marketing course is out now!', 'influencer-codewing'); ?>
                            <a href="#"><?php esc_html_e('Register Today', 'influencer-codewing'); ?></a>
                        </p>
                    </div>
                </div>
            </div><!-- .header-ticker -->
            <div class="main-header">
                <div class="cm-wrapper">
                    <div class="site-branding" itemscope itemtype="https://schema.org/Organization">
                        <?php if (has_custom_logo()) { ?>
                            <div class="site-logo">
                                <?php the_custom_logo(); ?>
                            </div><!-- .site-logo -->
                        <?php } ?>
                        <div>
                            <?php influencer_codewing_site_branding(); ?>
                        </div>
                    </div>
                    <?php influencer_codewing_menu(); ?>
                </div>
            </div><!-- .main-header -->
        </header>
    <?php 
    }
}

if ( ! function_exists( 'influencer_codewing_frontpage_sections' ) ) {
    /** 
     * Front page sections to display on static front page
     */
    function influencer_codewing_frontpage_sections() {
        $display_banner     =   get_theme_mod( 'influencer_codewing_frontpage_banner_handle');
        $blog_title         =   get_theme_mod( 'influencer_codewing_latest_posts_title' );
        $blog_subtitle      =   get_theme_mod( 'influencer_codewing_latest_posts_subtitle' );
        $blog_button        =   get_theme_mod( 'influencer_codewing_latest_posts_button' ); 
        if ( $display_banner ) { 
            influencer_codewing_header_image_display(); 
        } 
        if ( is_active_sidebar( 'logo-section' ) ) { ?>
            <section id="clients" class="client-section">
                <div class="cm-wrapper">
                    <?php dynamic_sidebar( 'logo-section' ); ?>
                </div>
            </section>
        <?php } 
        if ( is_active_sidebar( 'featured-page' ) ) { ?>
            <section class="about-section">
                <div class="cm-wrapper">
                    <?php dynamic_sidebar( 'featured-page' ); ?>
                </div>
            </section>
        <?php } 
        if ( is_active_sidebar( 'front-page-icon-text' ) ) { ?>
            <section class="service-section">
                <div class="cm-wrapper">
                    <div class="section-widget-wrap">
                        <?php dynamic_sidebar( 'front-page-icon-text' ); ?>
                    </div>
                </div>
            </section>
        <?php }
        if ( is_active_sidebar( 'front-page-testimonial' ) ) { ?>
            <section class="testimonial-section">
			    <div class="cm-wrapper">
				    <div class="section-widget-wrap">
                        <?php dynamic_sidebar( 'front-page-testimonial' ); ?>
                    </div>
                </div>
            </section>
        <?php } 
        if ( is_active_sidebar( 'front-page-cta' ) ) { ?>
            <section class="cta-section">
			    <div class="cm-wrapper">				    
                    <?php dynamic_sidebar( 'front-page-cta' ); ?>                 
                </div>
            </section>
        <?php } ?>
        <section class="news-section">
            <div class="cm-wrapper">
                <?php if ( $blog_subtitle ) { ?>
                    <p class="section-subtitle"><?php echo esc_html( $blog_subtitle ); ?></p>
                <?php } if ( $blog_title ) { ?>
                    <h1 class="section-title"><?php echo esc_html( $blog_title ); ?></h1>
                <?php } ?>
                <div class="news-block-wrap clearfix">
                    <?php 
                        $args   =   array(
                            'posts_per_page'    =>  3,
                            'post_type'         =>  'post',
                            'order'             =>  'DESC'
                        );
                        $qry    =   new WP_Query( $args );
                        if ( $qry->have_posts() ) {
                            while( $qry->have_posts() ) {
                                $qry->the_post(); ?>
                                <?php get_template_part( 'sections/latest-news', get_post_type() ); ?>
                            <?php }
                            wp_reset_postdata(); 
                        } 
                    ?> 
                </div>
                <?php if ( $blog_button ) { ?>
                    <a class="bttn" href="<?php echo esc_url( get_permalink( get_option( 'page_for_posts' ) ) ); ?>"><?php echo esc_html( $blog_button ); ?></a>
                <?php } ?>     
            </div>
        </section><!-- .news-section -->
        <?php 
    }
}

if ( ! function_exists( 'influencer_codewing_header_image_display' ) ) {
    /**
     * Displaying header image in the front page
     */
    function influencer_codewing_header_image_display() { ?>
        <div class="banner-section">
            <div class="banner-img">
                <div class="ban-img-holder"><?php the_custom_header_markup(); ?></div>
                <div class="cm-wrapper">
                    <img src="<?php echo esc_url(get_template_directory_uri() . '/images/ban-caption.png'); ?>" alt="<?php esc_html_e('banner caption', 'influencer-codewing'); ?>">
                </div>
                <a href="#clients" class="scroll-down"></a>
            </div>
        </div><!-- .banner-section -->
    <?php
    }
}

if ( ! function_exists( 'influencer_codewing_copyright' ) ) {
    /**
     * copyright text
     */
    function influencer_codewing_copyright() { ?>
        <div class="copyright">
            <?php
            $copyright_text    =    get_theme_mod('influencer_codewing_footer_text');
            if ($copyright_text) { ?>
                <?php echo wp_kses_post($copyright_text) ?>
            <?php } else { ?>
                <?php printf(esc_html__('&copy; Copyright %s', 'influencer-codewing'), date('Y')); ?>
            <?php } ?>
            <?php
            /* translators: 1: Theme name, 2: Theme author. */
            printf(esc_html__('Theme by %1$s.', 'influencer-codewing'), '<a href="https://rarathemes.com/">Rara Theme</a>');
            ?>
            <span class="sep"> | </span>
            <a href="<?php echo esc_url(__('https://wordpress.org/', 'influencer-codewing')); ?>">
                <?php
                /* translators: %s: CMS name, i.e. WordPress. */
                printf(esc_html__('Proudly powered by %s', 'influencer-codewing'), 'WordPress');
                ?>
            </a>
        </div>
    <?php
    }
}

if ( ! function_exists( 'influencer_codewing_blog_page_header' ) ) {
    /**
     * Displaying header for the blog page
     */
    function influencer_codewing_blog_page_header() {
        $image            =    get_theme_mod('influencer_codewing_index_header_image');
        $display_image    =    get_theme_mod('influencer_codewing_blog_header_handle');
        $image_text        =    get_theme_mod('influencer_codewing_index_header_image_title');

        if ($display_image && $image) { ?>
            <div class="page-header" style="background: url(<?php echo esc_url($image); ?>) no-repeat;">
                <div class="cm-wrapper">
                    <?php if ($image_text) { ?>
                        <h1 class="page-title"><?php echo esc_html($image_text); ?></h1>
                    <?php } ?>
                    <a href="#primary" class="scroll-down"></a>
                </div>
            </div>
        <?php } else { ?>
            <div style="padding: 75px 1019px 50px 1019px;"></div>
            <?php if ($image_text) { ?>
                <h1 style="text-align: center;"><?php echo esc_html($image_text); ?></h1>
            <?php }
        }
    }
}

if ( ! function_exists( 'influencer_codewing_single_page_header' ) ) {
    /**
     * Displaying header for the single page
     */
    function influencer_codewing_single_page_header() {
        global $post;
        $author_ID = $post->post_author;
        $image  =   get_theme_mod('influencer_codewing_single_header_img');
        $display_image  =   get_theme_mod('influencer_codewing_single_header_handle');

        if ($display_image && $image) { 
            $style = $image; 
        } elseif (has_post_thumbnail()) { 
            $style =  get_the_post_thumbnail_url(); 
        } ?>

        <div class="page-header" style="background: url(<?php echo esc_url($style); ?>); no-repeat;">
            <div class="cm-wrapper">
                <h1 class="page-title" itemprop="headline"><?php single_post_title(); ?></h1>
                <div class="entry-meta">
                    <span class="posted-on" itemprop="datePublished dateModified">
                        <?php esc_html_e('on', 'influencer-codewing'); ?> 
                        <a href="<?php echo esc_url( get_day_link( get_post_time( 'Y' ), get_post_time( 'm' ), get_post_time( 'j' ) ) ); ?>">
                            <time class="updated published"><?php echo esc_html(get_the_date('F j, Y')); ?></time>
                        </a>
                    </span>
                    <span class="byline" itemprop="author">
                        <?php esc_html_e('by', 'influencer-codewing'); ?> <span class="author vcard">
                            <a href="<?php echo esc_url( get_author_posts_url( $post->post_author ) ); ?>" class="url" itemprop="name"><?php the_author_meta('display_name', $author_ID); ?></a>
                        </span>
                    </span>
                    <span class="comment-box">
                        <span class="comment-count"><?php echo absint(get_comments_number()); ?></span> <?php esc_html_e('Comments', 'influencer-codewing'); ?>
                    </span>
                </div>
                <a href="#primary" class="scroll-down"></a>
            </div>
        </div>
    <?php
    }
}

if ( ! function_exists( 'influencer_codewing_author_newsletter_box' ) ) {
    /**
     * Author box and newsletter 
     */
    function influencer_codewing_author_newsletter_box() { ?>
        <div class="author-newsletter-wrap">
            <div class="about-author">
                <figure class="author-image"><?php echo get_avatar(get_the_ID(), 100, '', 'avatar', []); ?></figure>
                <h3 class="author-name"><?php esc_html_e('About the Author ', 'influencer-codewing'); ?><span><?php the_author(); ?></span></h3>
                <div class="author-desc">
                    <?php the_author_meta('description'); ?>
                </div>
                <div class="author-social-links">
                    <a href="#" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a>
                    <a href="#" target="_blank" class="twitter"><i class="fa fa-twitter"></i></a>
                    <a href="#" target="_blank" class="instagram"><i class="fa fa-instagram"></i></a>
                    <a href="#" target="_blank" class="snapchat"><i class="fa fa-snapchat-ghost"></i></a>
                    <a href="#" target="_blank" class="pinterest"><i class="fa fa-pinterest"></i></a>
                    <a href="#" target="_blank" class="google-plus"><i class="fa fa-google-plus"></i></a>
                </div>
            </div>
            <div class="post-newsletter">
                <img src="<?php echo esc_url(get_template_directory_uri() . '/images/form.jpg'); ?>" alt="<?php esc_attr_e('form', 'influencer-codewing'); ?>">
            </div>
        </div>
    <?php
    }
}

if ( ! function_exists( 'influencer_codewing_similar_posts' ) ) {
    /**
     *Similar posts display
     */
    function influencer_codewing_similar_posts() { ?>
        <div class="related-post">
            <h3 class="related-post-title"><?php esc_html_e('Related Articles', 'influencer-codewing'); ?></h3>
            <div class="related-post-wrap clearfix">
                <?php
                $num = (get_theme_mod('influencer_codewing_single_layout_handle') !== 'one') ? 6 : 4;
                global $post;
                $categories     =   get_the_category();
                $args           =   array(
                    'posts_per_page'    =>  $num,
                    'post__not_in'      =>  [$post->ID],
                    'cat'               =>  !empty($categories) ? $categories[0]->term_id : '',
                );
                $ra_query       =   new WP_Query($args);
                if ($ra_query->have_posts()) {
                    while ($ra_query->have_posts()) {
                        $ra_query->the_post();
                        get_template_part('inc/content/related-posts');
                    }
                    wp_reset_postdata();
                } ?>
            </div>
        </div>
    <?php
    }
}

if ( ! function_exists( 'influencer_codewing_comment' ) ) {
    /**
     *Callback function for comments list in posts
     */
    function influencer_codewing_comment($comment, $args, $depth) { ?>
        <li <?php comment_class(empty($args['has_children']) ? '' : 'parent'); ?> id="comment-<?php comment_ID(); ?>">
            <article class="comment-body" id="div-comment-<?php comment_ID(); ?>">
                <footer class="comment-meta">
                    <div class="comment-author vcard">
                        <?php if ($args['avatar_size'] != 0) echo get_avatar($comment, $args['avatar_size'], '', 'avatar', []); ?>
                        <b class="fn">
                            <a href="<?php echo esc_url(get_comment_author_url()); ?>"><?php comment_author(); ?></a>
                        </b>
                    </div><!-- .comment-author vcard -->

                    <div class="comment-metadata">
                        <a href="<?php echo esc_url(htmlspecialchars(get_comment_link($comment->comment_ID))); ?>">
                            <time><?php printf(esc_html__('%1$s at %2$s', 'influencer-codewing'), get_comment_date(),  get_comment_time()); ?></time>
                        </a>
                    </div>
                    <?php if ($comment->comment_approved == '0') { ?>
                        <p class="comment-awaiting-moderation"><?php esc_html_e('Your comment is awaiting moderation.', 'influencer-codewing'); ?></p>
                    <?php } ?>
                    <div class="reply">
                        <?php comment_reply_link(array_merge($args, array(
                            'add_below' => 'div-comment',
                            'depth'     => $depth,
                            'max_depth' => $args['max_depth'],
                        ))); ?>
                    </div>
                </footer>

                <div class="comment-content">
                    <p class="comment-content"><?php comment_text(); ?></p>
                </div><!-- .text-holder -->
            </article><!-- .comment-body -->
    <?php
    }
}

if ( ! function_exists( 'influencer_codewing_about_us_page_header' ) ) {
    /**
     *Page header incuding banner for about us page
     */
    function influencer_codewing_about_us_page_header() {
        $image = get_theme_mod( 'influencer_codewing_about_us_header_image', esc_url( get_theme_file_uri( 'images/blog-grid-img4.jpg' ) ) );
        $image ? $header_img = $image : $header_img = 'https://images.unsplash.com/photo-1591347888502-892420bdf7ea?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=1901&q=80';
        ?>        
        <div class="page-header" style="background: url(<?php echo esc_url( $header_img ); ?>) no-repeat;">
            <div class="cm-wrapper">
                <h1 class="page-title"><?php the_title(); ?></h1>
                <div class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
                    <div id="crumbs">
                        <a href="#" class="home_crumb"><?php esc_html_e( 'Home', 'influencer-codewing' ); ?></a> 
                        <span class="separator">&#9866;</span> 
                        <span class="current"><?php esc_html_e( 'About us', 'influencer-codewing' ); ?></span>
                    </div>
                </div>
            </div>
        </div>
        <?php    
    }
}

if ( ! function_exists( 'influencer_codewing_about_us_content' ) ) {
    /**
     *Content of about us page
     */
    function influencer_codewing_about_us_content() {
        $display_content = get_theme_mod( 'influencer_codewing_about_us_page_handle', 'yes' ); 
        $display_content_image = get_theme_mod ( 'influencer_codewing_about_us_page_image_handle', 'yes' );
        $content_image = get_theme_mod( 'influencer_codewing_about_us_content_image', esc_url( get_theme_file_uri( 'images/story-img.jpg' ) ) ); 
        if( $display_content || $display_content_image || $content_image) { ?>
            <div class="cm-wrapper">
                <div class="about-story">
                    <?php if ( $display_content ) { ?>
                        <div class="story-content-wrap">
                            <?php the_content(); ?>
                        </div>
                    <?php } 
                    if ( $display_content_image && $content_image ) { ?>
                        <figure class="story-img">
                            <img src="<?php echo esc_url( $content_image ); ?>" alt="<?php esc_html_e( 'story image', 'influencer-codewing' ); ?>">
                        </figure>
                    <?php } ?>
                </div>
            </div>
        <?php }
    }
}

if ( ! function_exists( 'influencer_codewing_about_us_media_widget' ) ) {
    /**
     * Image and media widget in about us page
     */
    function influencer_codewing_about_us_media_widget() {
        if ( is_active_sidebar( 'media-image' ) || is_active_sidebar( 'icon-text' ) ) { ?>
            <div class="story-feat-wrap clearfix">
                <?php if ( is_active_sidebar( 'media-image' ) ) { 
                    dynamic_sidebar( 'media-image' );
                } 
                if ( is_active_sidebar( 'icon-text' ) ) { ?>
                    <div class="widget-wrap">
                        <?php dynamic_sidebar( 'icon-text' ); ?>
                    </div>
                <?php } ?>
            </div>
        <?php }
    }
}

if ( ! function_exists( 'influencer_codewing_about_us_success_stats' ) ) {
    /**
     * Success stats counter widget in about us page
     */
    function influencer_codewing_about_us_success_stats() {
        if ( is_active_sidebar( 'success-stats' ) ) { 
            $bg_image = get_theme_mod( 'influencer_codewing_about_us_stats_count_bg_image', esc_url( get_theme_file_uri( 'images/stat-counter-bg.jpg' ) ) );
            $bg_image ? $image = $bg_image : $image = 'https://images.unsplash.com/photo-1591347888502-892420bdf7ea?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=1901&q=80' ;
            ?>
            <section class="story-statcounter-section" style="background: url(<?php echo esc_url( $image ); ?>) no-repeat;">
                <div class="cm-wrapper">
                    <div class="section-widget-wrap">
                        <?php dynamic_sidebar( 'success-stats' ); ?>                    
                    </div>
                </div>
            </section>
        <?php } 
    }
}

if ( ! function_exists( 'influencer_codewing_about_us_team' ) ) {
    /**
     * Team widget in about us page
     */
    function influencer_codewing_about_us_team() {
        if ( is_active_sidebar( 'team-member' ) ) { ?>
            <section class="story-team-section">
                <div class="cm-wrapper">
                    <div class="section-widget-wrap">
                        <?php dynamic_sidebar( 'team-member' ); ?>                   
                    </div>
                </div>
            </section>
        <?php }
    }
}

if ( ! function_exists( 'influencer_codewing_podcast_page_header' ) ) {
    /**
     * Page header for podcast page template
     */
    function influencer_codewing_podcast_page_header() {
        $image = get_theme_mod( 'influencer_codewing_podcast_header_image' );
        $image_title = get_theme_mod( 'influencer_codewing_podcast_header_title', esc_html__( 'Listen to our awesome Podcasts', 'influencer-codewing' ) );
        ?>
        <div class="page-header" style="background: url(<?php echo esc_url($image); ?>) no-repeat;">
            <div class="cm-wrapper">
                <?php if ($image_title) { ?>
                    <h1 class="page-title"><?php echo esc_html( $image_title ); ?></h1>
                <?php } ?>
                <div class="breadcrumb">
                    <a class="breadcrumb-item" href="#"><?php esc_html_e( 'Home', 'influencer-codewing' ); ?></a>
                    <span class="seperator"><i class="fa fa-caret-right"></i></span>
                    <span class="breadcrumb-item current"><?php esc_html_e( 'Podcast', 'influencer-codewing' ); ?></span>
                </div>
                <a href="#primary" class="scroll-down"></a>
            </div>
        </div>
        <?php
    }
}