<?php

/**
 * Influencer Codewing functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Influencer_Codewing
 */

if ( ! defined( '_INFLUENCER_CODEWING' ) ) {
	// Replace the version number of the theme on each release.
	define( '_INFLUENCER_CODEWING', '1.0.0' );
}

if ( ! function_exists( 'influencer_codewing_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function influencer_codewing_setup()
	{
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Influencer Codewing, use a find and replace
		 * to change 'influencer-codewing' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'influencer-codewing', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'primary' => esc_html__( 'Primary', 'influencer-codewing' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'influencer_codewing_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		/**
		 * Add support for custom header image.
		 */

		$args = array(
			'width'         =>  1920,
			'height'        =>  999,
			'video'         =>  true,
			'header-text'   =>  false,
			'default-text-color'     => '',
			'default-image' => esc_url( get_template_directory_uri() . '/images/banner-img1.jpg' ),
		);
		add_theme_support( 'custom-header', apply_filters( 'influencer_codewing_banner', $args ) );

		register_default_headers( array(
			'default-banner' => array(
					'url'           => '%s/images/banner-img1.jpg',
					'thumbnail_url' => '%s/images/banner-img1.jpg',
					'description'   => esc_html__( 'Header Image description', 'influencer-codewing' ),
				), 
			)    
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);

		/**
		 * Adding custom thumbnail sizes
		 */
		add_image_size( 'influencer_codewing_frontpage_blog', 370, 220, true );
		add_image_size( 'influencer_codewing_blog_grid_layout', 370, 149, true );
		add_image_size( 'influencer_codewing_blog_grid_second_layout', 308, 124, true );
		add_image_size( 'influencer_codewing_blog_grid_third_layout', 806, 323, true );
		add_image_size( 'influencer_codewing_related_posts', 390, 232, true );

	}
endif;
add_action( 'after_setup_theme', 'influencer_codewing_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function influencer_codewing_content_width()
{
	$GLOBALS['content_width'] = apply_filters( 'influencer_codewing_content_width', 640 );
}
add_action( 'after_setup_theme', 'influencer_codewing_content_width', 0 );

/**
 * Enqueue scripts and styles.
 */
function influencer_codewing_scripts() {

	wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css', array(), '4.7.0' );
	wp_enqueue_style( 'influencer-codewing-style', get_stylesheet_uri(), array(), _INFLUENCER_CODEWING );
	wp_style_add_data( 'influencer-codewing-style', 'rtl', 'replace' );

	wp_enqueue_script( 'influencer-codewing-jquery-counterup', get_template_directory_uri() . '/js/jquery.counterup.js', array('jquery'), _INFLUENCER_CODEWING, true );
	wp_enqueue_script( 'waypoints', get_template_directory_uri() . '/js/waypoints-2.0.3.js', array('jquery'), '2.0.3', true );
	wp_enqueue_script( 'influencer-codewing-custom', get_template_directory_uri() . '/js/custom.js', array('jquery'), _INFLUENCER_CODEWING, true );
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
		
	}
}
add_action( 'wp_enqueue_scripts', 'influencer_codewing_scripts' );

/**
 * Add custom post type
 */
function influencer_codewing_custom_post_type() {
 
	$labels = array(
		'name'                => esc_html_x( 'Podcasts', 'Post Type General Name', 'influencer-codewing' ),
		'singular_name'       => esc_html_x( 'Podcast', 'Post Type Singular Name', 'influencer-codewing' ),
		'menu_name'           => esc_html__( 'Podcasts', 'influencer-codewing' ),
		'parent_item_colon'   => esc_html__( 'Parent Podcast', 'influencer-codewing' ),
		'all_items'           => esc_html__( 'All Podcasts', 'influencer-codewing' ),
		'view_item'           => esc_html__( 'View Podcast', 'influencer-codewing' ),
		'add_new_item'        => esc_html__( 'Add New Podcast', 'influencer-codewing' ),
		'add_new'             => esc_html__( 'Add New', 'influencer-codewing' ),
		'edit_item'           => esc_html__( 'Edit Podcast', 'influencer-codewing' ),
		'update_item'         => esc_html__( 'Update Podcast', 'influencer-codewing' ),
		'search_items'        => esc_html__( 'Search Podcast', 'influencer-codewing' ),
		'not_found'           => esc_html__( 'Not Found', 'influencer-codewing' ),
		'not_found_in_trash'  => esc_html__( 'Not found in Trash', 'influencer-codewing' ),
	);
	 
	$args = array(
		'label'               => esc_html__( 'Podcasts', 'influencer-codewing' ),
		'description'         => esc_html__( 'Podcast', 'influencer-codewing' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
		'hierarchical'        => true,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 25,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
		'show_in_rest'        => true,
		'menu_icon'           => 'dashicons-controls-play',
 
	);
	 
	register_post_type( 'podcasts', $args );

	//Create taxonomy
	
	$catlabels = array(
		'name'              => esc_html_x( 'Categories', 'taxonomy general name', 'influencer-codewing' ),
		'singular_name'     => esc_html_x( 'Category', 'taxonomy singular name', 'influencer-codewing' ),
		'search_items'      => esc_html__( 'Search Categories', 'influencer-codewing' ),
		'all_items'         => esc_html__( 'All Categories', 'influencer-codewing' ),
		'parent_item'       => esc_html__( 'Parent Category', 'influencer-codewing' ),
		'parent_item_colon' => esc_html__( 'Parent Category:', 'influencer-codewing' ),
		'edit_item'         => esc_html__( 'Edit Category', 'influencer-codewing' ),
		'update_item'       => esc_html__( 'Update Category', 'influencer-codewing' ),
		'add_new_item'      => esc_html__( 'Add New Category', 'influencer-codewing' ),
		'new_item_name'     => esc_html__( 'New Category Name', 'influencer-codewing' ),
		'menu_name'         => esc_html__( 'Categories', 'influencer-codewing' ),
	);    
	
	// Register the taxonomy
	register_taxonomy( 'categoryitems', array( 'items' ), array(
		'hierarchical'      => true,
		'labels'            => $catlabels,
		'show_ui'           => true,
		'show_in_rest'      => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'category-item' ),
	));
 
}

add_action( 'init', 'influencer_codewing_custom_post_type' );