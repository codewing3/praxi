<?php
/**
 * The main template file
 *
 * The Site Front Page template file
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Influencer_Codewing
 */

get_header();

if ( 'posts' == get_option( 'show_on_front' ) ) { //Show Static Blog Page   
     include( get_home_template() ); 
} else { 
    influencer_codewing_frontpage_sections();
} 

get_footer();
