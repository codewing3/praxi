<?php
/**
 * The template for displaying comments
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Influencer_Codewing
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>
<div class="comments-area" itemscope itemtype="http://schema.org/Comment">
    <?php if( have_comments() ) { ?>
        <h2 class="comment-title"><?php echo absint( get_comments_number() ); esc_html_e( ' Comments:', 'influencer-codewing' ); ?></h2>
        <ol class="comment-list">
            <?php
				wp_list_comments( array(
					'style'         =>  'ol',
					'short_ping'    =>  true,
                    'callback'      =>  'influencer_codewing_comment',
                    'avatar_size'   =>  35,
				) );
			?>
        </ol>
    <?php } 
    comment_form(); ?>
</div>