<?php

/** Template name: Podcast
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Influencer_Codewing
 */

get_header();
/**
 * Page header for the podcast page template
 */
influencer_codewing_podcast_page_header();
?>
<div class="cm-wrapper">
    <div id="primary" class="content-area">
        <main id="main" class="site-main">
            <?php
            $args = array(
                'post_type' => 'podcasts',
                'posts_per_page' => 5,
            );
            $pod_qry = new WP_Query($args);
            if ($pod_qry->have_posts()) {
            ?>
                <div class="article-group list-layout">
                    <?php while ($pod_qry->have_posts()) {
                        $pod_qry->the_post();
                        get_template_part('template-parts/content', get_post_type());
                    } wp_reset_postdata(); ?>
                </div>         
            <?php } else {
                get_template_part('template-parts/content', 'none');
            } ?>
        </main>
    </div>
</div>
<?php
get_footer();
