<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Influencer_Codewing
 */

get_header();
$image = get_theme_mod( 'influencer_codewing_archive_header_image' );
?>
<div class="page-header" style="background: url(<?php echo esc_url( $image ); ?>);">
    <?php
    the_archive_title( '<h1 class="page-title">', '</h1>' );
    ?>
</div><!-- .page-header -->
<div class="cm-wrapper">
    <div id="primary" class="content-area">	
        <main id="main" class="site-main">
            <?php if ( have_posts() ) : ?>
                <?php
                /* Start the Loop */
                while ( have_posts() ) :
                    the_post();
                    /*
                    * Include the Post-Type-specific template for the content.
                    * If you want to override this in a child theme, then include a file
                    * called content-___.php (where ___ is the Post Type name) and that will be used instead.
                    */
                    get_template_part( 'template-parts/content', 'archive' );

                endwhile;
                the_posts_navigation();
            else :
                get_template_part( 'template-parts/content', 'none' );
            endif;
            ?>
	    </main><!-- #main -->
    </div>
    <?php get_sidebar(); ?>
</div>
<?php
get_footer();
