<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Influencer_Codewing
 */

get_header();
?>

<div id="content" class="site-content">
    <?php 
    /**
     * Displaying single page header with image
     */
     influencer_codewing_single_page_header(); 
     ?>
    <div class="cm-wrapper">
        <div id="primary" class="content-area">
            <main id="main" class="site-main">
                <div class="article-group">
                    <?php if ( have_posts() ){ 
                        while ( have_posts () ){
                            the_post(); 
                            get_template_part( 'template-parts/content', 'single' ); 
                        } 
                    } ?>
                </div>
                <?php 
                /**
                 * Displaying author box and newsletter
                 */
                influencer_codewing_author_newsletter_box(); 
                
                the_post_navigation( [
                    'prev_text'          => sprintf( esc_html__( '%1s &#129120; Previous Posts %2s', 'influencer-codewing'), '<span class="pagination-txt">', '</span><h4>%title</h4>'),
                    'next_text'          => sprintf( esc_html__( '%1s Next Posts &#129122; %2s', 'influencer-codewing'), '<span class="pagination-txt">', '</span><h4>%title</h4>'),
                ]);           
                
                $single_layout = get_theme_mod( 'influencer_codewing_single_layout_handle' ); 
                /**
                 * Displaying similar posts before comments on layout one
                 */
                ( $single_layout === 'one') ? influencer_codewing_similar_posts() : ''; 
                
                if( comments_open() || get_comments_number() ){ 
                    comments_template();
                }                            
                ?>
            </main>
        </div>
        <?php 
        /**
         * Displaying sidebar only on layout one for single posts
         */
        ( $single_layout === 'one' ) ? get_sidebar() : ''; 
        /**
         * Displaying similar posts after comments on layout two and three
         */
        ( $single_layout !== 'one') ? influencer_codewing_similar_posts() : '';
        ?>
    </div>
</div>
<?php get_footer(); ?>