<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Influencer_Codewing
 */

?>

	<footer id="colophon" class="site-footer">		
		<?php if ( is_active_sidebar( 'footer' ) || is_active_sidebar( 'footer-2' ) || is_active_sidebar( 'footer-3' ) || is_active_sidebar( 'footer-4' ) ) { ?>
            <div class="top-footer">    
                <div class="cm-wrapper">
				    <?php 
						if ( is_active_sidebar( 'footer-1' ) ) {
							dynamic_sidebar( 'footer-1' );
						} 
						if ( is_active_sidebar( 'footer-2' ) ) {
							dynamic_sidebar( 'footer-2' );	
						}
						if ( is_active_sidebar( 'footer-3' ) ) {
							dynamic_sidebar( 'footer-3' );	
						} 
						if ( is_active_sidebar( 'footer-4' ) ) {
							dynamic_sidebar( 'footer-4' );
						}
					?>
                </div>
            </div><!-- .top-footer -->
        <?php } ?>  
		<div class="bottom-footer">
			<div class="cm-wrapper">
				<?php influencer_codewing_copyright(); ?>
				<div class="scroll-to-top">
					<span><?php esc_html_e( 'back to top', 'influencer-codewing' ); ?><img src=<?php echo esc_url( get_template_directory_uri() . "/images/scroll-top-arrow.png" ); ?> alt="<?php esc_html_e( 'scroll to top', 'influencer-codewing' ); ?>"></span>
				</div>
			</div>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
