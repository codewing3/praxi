<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Influencer_Codewing
 */

get_header();
$image = get_theme_mod( 'influencer_codewing_404_header_image' );
?>
<div class="page-header" style="background: url(<?php echo esc_url( $image ); ?>);" >
    <h1 class="page-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'influencer-codewing' ); ?></h1>
</div><!-- .page-header -->
<div class="cm-wrapper">
    <div id="primary" class="content-area">	
        <main id="main" class="site-main">
            <section class="error-404 not-found">
                <div class="page-content">
                    <p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'influencer-codewing' ); ?></p>
                    <?php
                    get_search_form();
                    the_widget( 'WP_Widget_Recent_Posts' );
                    ?>
                    <div class="widget widget_categories">
                        <h2 class="widget-title"><?php esc_html_e( 'Most Used Categories', 'influencer-codewing' ); ?></h2>
                        <ul>
                            <?php
                            wp_list_categories(
                                array(
                                    'orderby'    => 'count',
                                    'order'      => 'DESC',
                                    'show_count' => 1,
                                    'title_li'   => '',
                                    'number'     => 10,
                                )
                            );
                            ?>
                        </ul>
                    </div><!-- .widget -->
                </div><!-- .page-content -->
            </section><!-- .error-404 -->
	    </main><!-- #main -->
    </div>
</div>

<?php
get_footer();
