<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Influencer_Codewing
 */

get_header(); 
$image = get_theme_mod( 'influencer_codewing_page_header_image', esc_url( get_theme_file_uri( 'images/blog-grid-img4.jpg' ) ) );
?>
<div id="content" class="site-content">
    <div class="page-header" style="background: url(<?php echo esc_url( $image ); ?>) no-repeat;">
        <div class="cm-wrapper">
            <h1 class="page-title" itemprop="headline"><?php the_title(); ?></h1>
            <a href="#primary" class="scroll-down"></a>
        </div>
    </div>
    <div class="cm-wrapper">
        <div id="primary" class="content-area">
            <main id="main" class="site-main">
                <div class="article-group">
                    <?php
                    while ( have_posts() ) : the_post();

                        get_template_part( 'template-parts/content', 'page' );

                    endwhile; // End of the loop.
                    ?>
                </div>
            </main><!-- #main -->
        </div><!-- #primary -->
        <?php get_sidebar(); ?>
    </div>
</div>
<?php get_footer(); ?>