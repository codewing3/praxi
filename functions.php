<?php
/**
 * Influencer Codewing functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Influencer_Codewing
 */
 /**
 * Implement the Custom feature and add post_type.
 */
require get_template_directory() . '/inc/custom-functions.php';
/**
 * Widgets codes.
 */
require get_template_directory() . '/inc/widgets.php';
/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
$customizer = [ 'customizer.php', 'partials.php', 'footer.php', 'frontpage/banner.php', 'frontpage/latestposts.php', 'frontpage/frontpage-widgets.php', 'header-images/blog.php', 'header-images/single-post.php', 'header-images/search.php', 'header-images/404.php', 'header-images/archive.php', 'header-images/page.php', 'layout/index-layout.php', 'layout/single-layout.php', 'sanitize-callback.php', 'header-images/podcasts.php', 'about-us/about-us.php', 'about-us/widgets-customizer.php' ];
foreach( $customizer as $value ) {
	require get_template_directory() . '/inc/customizer/' . $value;
}
/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}