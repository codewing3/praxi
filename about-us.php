<?php
/**
 * Template name: About 
 */
get_header();
?>
<div id="content" class="site-content">
    <?php 
    /**
     * Displaying page header which has background image
     */
    influencer_codewing_about_us_page_header(); 
    ?>
    <div id="primary" class="content-area">
        <main id="main" class="site-main">
            <?php 
            /**
            * Displaying content 
            */
            influencer_codewing_about_us_content(); 
            /**
            * Display media image and icon text widgets
            */
            influencer_codewing_about_us_media_widget();
            /**
             * Displaying success stats widget
             */
            influencer_codewing_about_us_success_stats();
            /**
             * Displaying team widget
             */
            influencer_codewing_about_us_team()
            ?>
        </main>
    </div>
</div>
<?php
get_footer(); 